<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AA_UUP_LastNameInput</name>
   <tag></tag>
   <elementGuidId>7683b2aa-827a-4d09-8a61-6dcc5b030ba3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),'Last Name')]//parent::nobr/..//following-sibling::td/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),'Last Name')]//parent::nobr/..//following-sibling::td/input</value>
   </webElementProperties>
</WebElementEntity>
