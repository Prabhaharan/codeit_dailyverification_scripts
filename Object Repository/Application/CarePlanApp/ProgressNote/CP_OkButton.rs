<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CP_OkButton</name>
   <tag></tag>
   <elementGuidId>7bc70ac8-84aa-467b-a412-7c8c3c1e6f2e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@value=&quot;OK&quot; and @id=&quot;ATLC_OK_but&quot;]</value>
   </webElementProperties>
</WebElementEntity>
