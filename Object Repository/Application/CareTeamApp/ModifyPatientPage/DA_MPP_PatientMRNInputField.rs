<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DA_MPP_PatientMRNInputField</name>
   <tag></tag>
   <elementGuidId>639458d8-e0f1-4071-9ff3-c7535bb8fa0e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'firstNameTextItem']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>firstNameTextItem</value>
   </webElementProperties>
</WebElementEntity>
