<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>createNewCCPButton</name>
   <tag></tag>
   <elementGuidId>95dea033-9484-485d-9bec-3aa3d0c6c7ea</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[text()='Create New CCP Issue/Need']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[text()='Create New CCP Issue/Need']</value>
   </webElementProperties>
</WebElementEntity>
