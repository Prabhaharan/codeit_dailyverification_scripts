<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CPA_EIP_EELP_EditEncounterIcon</name>
   <tag></tag>
   <elementGuidId>437d13eb-fb4d-47a5-9361-01a22e7a223a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//img[@title='Edit Encounter']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//img[@title='Edit Encounter']</value>
   </webElementProperties>
</WebElementEntity>
