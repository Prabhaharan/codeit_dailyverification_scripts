<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>existingInterventionCheck</name>
   <tag></tag>
   <elementGuidId>f7ec0978-4a21-493c-9069-230876652738</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//td[contains(text(),&quot;Interventions&quot;)and not(contains(text(),&quot;(0)&quot;))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[contains(text(),&quot;Interventions&quot;)and not(contains(text(),&quot;(0)&quot;))]</value>
   </webElementProperties>
</WebElementEntity>
