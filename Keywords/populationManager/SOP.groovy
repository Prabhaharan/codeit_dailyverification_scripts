package populationManager

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class SOP {
	@Keyword
	public Boolean alertReportPageNavigation(){
		try{
			WebUI.doubleClick(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/AlertReport'), FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30, FailureHandling.STOP_ON_FAILURE)
			WebUI.switchToFrame(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30,FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/FromDate'),50, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return true
		} catch(Exception e){
			return false
		}
	}

	@Keyword
	public Boolean assessmentReportPageNavigation(){
		try{
			// Navigate to Assessment Report
			WebUI.doubleClick(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/AssessmentReport'), FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30, FailureHandling.STOP_ON_FAILURE)
			WebUI.switchToFrame(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30,FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/FromDate'),50, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return true
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean bTQDemographicReportPageNavigation(){
		try{
			// Navigate to BTQ Demographic Report
			WebUI.doubleClick(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/BTQDemographicReport'), FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30, FailureHandling.STOP_ON_FAILURE)
			WebUI.switchToFrame(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30,FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/FromDate'),50, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return true
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean bTQEncounterActivityReportPageNavigation(){
		try{
			// Navigate to BTQ Demographic Report
			WebUI.doubleClick(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/BTQEncounterActivityReport'), FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30, FailureHandling.STOP_ON_FAILURE)
			WebUI.switchToFrame(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30,FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/FromDate'),50, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return true
		} catch(Exception e){
			return false
		}
	}

	@Keyword
	public Boolean CarePlanEncounterActivityReportPageNavigation(){
		try{
			// Navigate to CarePlan Encounter Report
			WebUI.doubleClick(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/CarePlanEncounterReport'), FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30, FailureHandling.STOP_ON_FAILURE)
			WebUI.switchToFrame(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30,FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Object Repository/Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/FromAdmitDate'),50, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return true
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean CarePlanGapReportPageNavigation(){
		try{
			// Navigate to CarePlan Gap Report
			WebUI.doubleClick(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/CarePlanGapReport'), FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30, FailureHandling.STOP_ON_FAILURE)
			WebUI.switchToFrame(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30,FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/FromDate'),50, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return true
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean CareTeamReportPageNavigation(){
		try{
			// Navigate to CarePlan Gap Report
			WebUI.doubleClick(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/CareTeamReport'), FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30, FailureHandling.STOP_ON_FAILURE)
			WebUI.switchToFrame(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30,FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Object Repository/Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/CareTeam'),50, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return true
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean HealthixAlertsReportPageNavigation(){
		try{
			// Navigate to Healthix Alerts Report
			WebUI.doubleClick(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/HealthixAlert'), FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30, FailureHandling.STOP_ON_FAILURE)
			WebUI.switchToFrame(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30,FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/FromDate'),50, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return true
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean IssuesReportPageNavigation(){
		try{
			// Navigate to Issues Report
			WebUI.doubleClick(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/IssuesReport'), FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30, FailureHandling.STOP_ON_FAILURE)
			WebUI.switchToFrame(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30,FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/FromDate'),50, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return true
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean PatientProgramInformationReportPageNavigation(){
		try{
			// Navigate to Patient Program Information Report
			WebUI.doubleClick(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PatientProgramInformationReport'), FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30, FailureHandling.STOP_ON_FAILURE)
			WebUI.switchToFrame(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30,FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/FromDate'),50, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return true
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean ProgramEnrollmentReportPageNavigation(){
		try{
			// Navigate to Program Enrollment Report
			WebUI.doubleClick(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/ProgramEnrollmentReport'), FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30, FailureHandling.STOP_ON_FAILURE)
			WebUI.switchToFrame(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30,FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/FromDate'),50, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return true
		} catch(Exception e){
			return false
		}
	}
}


