<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CT_Bottom_Searchbutton</name>
   <tag></tag>
   <elementGuidId>de8edff9-b7fb-4668-a7c9-4f143c10f08e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[normalize-space(text()) ='Search'])[2]</value>
   </webElementProperties>
</WebElementEntity>
