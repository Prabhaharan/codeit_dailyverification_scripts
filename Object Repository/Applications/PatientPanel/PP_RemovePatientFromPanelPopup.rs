<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PP_RemovePatientFromPanelPopup</name>
   <tag></tag>
   <elementGuidId>0b40e157-98c4-40db-bdae-b20c4e1a59f9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(text(),'Remove')]//following::div//div//div//following::div/div[text()='Yes'] | //span[text()='Yes']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(text(),'Remove')]//following::div//div//div//following::div/div[text()='Yes'] | //span[text()='Yes']</value>
   </webElementProperties>
</WebElementEntity>
