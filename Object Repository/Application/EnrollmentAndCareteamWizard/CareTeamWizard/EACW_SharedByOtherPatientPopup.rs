<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_SharedByOtherPatientPopup</name>
   <tag></tag>
   <elementGuidId>da7f807e-b125-40f7-86f2-d4e5884fc10f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h2[@class='md-title ng-binding'][text()='Warning!']</value>
   </webElementProperties>
</WebElementEntity>
