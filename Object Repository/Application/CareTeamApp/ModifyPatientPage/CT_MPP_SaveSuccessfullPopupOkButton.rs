<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CT_MPP_SaveSuccessfullPopupOkButton</name>
   <tag></tag>
   <elementGuidId>29a5e16d-c40d-47e0-8878-5fc8d2fe19f0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[text()='Save successful.']//following::div[text()='OK']</value>
   </webElementProperties>
</WebElementEntity>
