<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>NextButton</name>
   <tag></tag>
   <elementGuidId>4ab10089-5b66-4762-b0c4-1f6730ab77df</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[contains(text(), 'Next')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[contains(text(), 'Next')]</value>
   </webElementProperties>
</WebElementEntity>
