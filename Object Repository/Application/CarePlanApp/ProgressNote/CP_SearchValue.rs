<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CP_SearchValue</name>
   <tag></tag>
   <elementGuidId>8276e5f3-6f6c-4b94-a77b-5baae41224a6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//option[contains(text(),'Contains Any')]/../../input</value>
   </webElementProperties>
</WebElementEntity>
