<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_DuplicateCareTeamNamePopUp</name>
   <tag></tag>
   <elementGuidId>be59eb25-eaab-4d0c-9bb9-324c53c97e8e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[text()='Care Team name already exists. Please enter a unique name']</value>
   </webElementProperties>
</WebElementEntity>
