<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DA_VPP_addPatientToPatientPanelSuccessfullOkButton</name>
   <tag></tag>
   <elementGuidId>57bdf8f5-4be6-4fc8-813e-9d097bb67ffc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[contains(text(),'Patient added to your patient panel.')]/../following::*/*[contains(text(),'OK')]</value>
   </webElementProperties>
</WebElementEntity>
