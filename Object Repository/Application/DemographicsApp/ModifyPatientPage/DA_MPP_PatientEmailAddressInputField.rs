<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DA_MPP_PatientEmailAddressInputField</name>
   <tag></tag>
   <elementGuidId>4f07b024-326d-4bf3-b5ea-5d9ba063df2e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'emailTextItem']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>emailTextItem</value>
   </webElementProperties>
</WebElementEntity>
