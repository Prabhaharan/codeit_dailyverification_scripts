<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AA_GetText_LastName</name>
   <tag></tag>
   <elementGuidId>541cdf44-3c49-4201-bfd7-1f8fe670867b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'isc_TextItem_12']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()='Last Name']//parent::nobr/..//following-sibling::td/input</value>
   </webElementProperties>
</WebElementEntity>
