<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CPA_EIP_EncounterIDInViewList</name>
   <tag></tag>
   <elementGuidId>1daea271-5546-46d8-967b-53264fc7ddf1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//tbody/tr[1]/td[2][@title=&quot;Click to View Encounter Details&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//tbody/tr[1]/td[2][@title=&quot;Click to View Encounter Details&quot;]</value>
   </webElementProperties>
</WebElementEntity>
