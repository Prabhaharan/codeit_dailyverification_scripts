<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MS_HP_LogoutMessageappWindow</name>
   <tag></tag>
   <elementGuidId>f375dbe9-b44c-4ee4-899d-44e57d63f488</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td/div/a[text()='Logout']</value>
   </webElementProperties>
</WebElementEntity>
