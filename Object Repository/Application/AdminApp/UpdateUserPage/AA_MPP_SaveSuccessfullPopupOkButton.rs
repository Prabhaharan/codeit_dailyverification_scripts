<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AA_MPP_SaveSuccessfullPopupOkButton</name>
   <tag></tag>
   <elementGuidId>2bf566e1-0fd8-475e-b35a-3d36edc48fd0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[text()='Save successful.']//following::div[text()='OK']</value>
   </webElementProperties>
</WebElementEntity>
