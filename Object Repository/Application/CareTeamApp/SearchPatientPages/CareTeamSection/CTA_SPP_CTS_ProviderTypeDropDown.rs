<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CTA_SPP_CTS_ProviderTypeDropDown</name>
   <tag></tag>
   <elementGuidId>e4da8c59-fb94-47f9-893a-b0795cc6730c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),&quot;Provider Type&quot;)]/..//following-sibling::td/table/tbody/tr/td[@class=&quot;comboBoxItemPickerCell&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>providerType</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),&quot;Provider Type&quot;)]/..//following-sibling::td/table/tbody/tr/td[@class=&quot;comboBoxItemPickerCell&quot;]</value>
   </webElementProperties>
</WebElementEntity>
