<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CTA_SPP_ResetAppButton</name>
   <tag></tag>
   <elementGuidId>16e26ab5-0edb-4870-ad83-8186276f894f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@aria-label=&quot;Reset App&quot;]</value>
   </webElementProperties>
</WebElementEntity>
