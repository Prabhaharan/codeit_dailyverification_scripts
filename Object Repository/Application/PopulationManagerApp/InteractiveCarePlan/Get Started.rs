<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Get Started</name>
   <tag></tag>
   <elementGuidId>e9138f9a-34f2-4143-90ca-88a958fecd3e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;pentaho-dialog dijitDialog dijitDialogFocused dijitFocused&quot;]//following::button[text()='Get Started']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;pentaho-dialog dijitDialog dijitDialogFocused dijitFocused&quot;]//following::button[text()='Get Started']</value>
   </webElementProperties>
</WebElementEntity>
