<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MS_Trash_DeletePopupLabel</name>
   <tag></tag>
   <elementGuidId>16e8c661-d7f6-4fa8-8174-93f0e9bd7b2f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(text(),'Are you sure you want to delete the selected messages?')]</value>
   </webElementProperties>
</WebElementEntity>
