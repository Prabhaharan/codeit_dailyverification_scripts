<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_DuplicateCareTeamNamePopUpYes</name>
   <tag></tag>
   <elementGuidId>f087f2ec-f72f-4ebd-a3f4-313f8cdbd3af</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[text()='Care Team name already exists. Please enter a unique name']/following::span[text()='ok']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[text()='Care Team name already exists. Please enter a unique name']/following::span[text()='ok']</value>
   </webElementProperties>
</WebElementEntity>
