<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DA_VPP_addPatientToPatientPanelSuccessfullOkButton</name>
   <tag></tag>
   <elementGuidId>02315c5e-b405-4d7f-a524-f1c9f4a8d708</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[contains(text(),'Patient added to your patient panel.')]/../following::*/*[contains(text(),'OK')]</value>
   </webElementProperties>
</WebElementEntity>
