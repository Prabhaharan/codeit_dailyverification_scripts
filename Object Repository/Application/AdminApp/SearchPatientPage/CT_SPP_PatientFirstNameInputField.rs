<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CT_SPP_PatientFirstNameInputField</name>
   <tag></tag>
   <elementGuidId>295856e5-e4b1-4350-bdc8-5d9c566b9df2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'firstName']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>firstName</value>
   </webElementProperties>
</WebElementEntity>
