<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>selectAllStatusInFilter</name>
   <tag></tag>
   <elementGuidId>12117746-99f6-4471-a57f-b1932476d924</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//b[text()='Issue Status']/following::a[text()='All']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//b[text()='Issue Status']/following::a[text()='All']</value>
   </webElementProperties>
</WebElementEntity>
