<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_CTW_ADNMW_FilterByRoleButton</name>
   <tag></tag>
   <elementGuidId>510e6693-6e4c-4774-a6d7-1e8e493d6700</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//md-select[@placeholder=&quot;Filter by user role&quot;]</value>
   </webElementProperties>
</WebElementEntity>
