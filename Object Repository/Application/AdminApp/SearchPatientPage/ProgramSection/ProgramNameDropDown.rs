<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ProgramNameDropDown</name>
   <tag></tag>
   <elementGuidId>7fa5b134-f4c4-43fd-8c4a-73d4c47572d8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),&quot;Program Name&quot;)]/..//following-sibling::td/table/tbody/tr/td[@class=&quot;comboBoxItemPickerCell&quot;]</value>
   </webElementProperties>
</WebElementEntity>
