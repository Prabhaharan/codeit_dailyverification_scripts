<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RHIO_EnabledIconInEnabledMode</name>
   <tag></tag>
   <elementGuidId>7b9f1bb6-c3b3-463d-b0db-8849701695ad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//md-switch[(@name=&quot;sharing_switch&quot;)  and (@aria-checked=&quot;true&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
