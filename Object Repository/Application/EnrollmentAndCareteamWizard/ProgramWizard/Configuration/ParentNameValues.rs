<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ParentNameValues</name>
   <tag></tag>
   <elementGuidId>a273405a-eb7c-4736-bd07-dd985e3eab61</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//md-option[contains(@ng-disabled,&quot;ParentProgram&quot;)]//parent::*</value>
   </webElementProperties>
</WebElementEntity>
